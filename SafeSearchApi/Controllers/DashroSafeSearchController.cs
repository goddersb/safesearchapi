﻿using SafeSearchApi.Models.Api;
using SafeSearchApi.Models.Configuration;
using SafeSearchApi.Models.Enumerated_Types;
using SafeSearchApi.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace SafeSearchApi.Controllers
{
    [ApiController]
    [Route("api/v1/SafeSearch")]
    [Authorize]
    public class DashroSafeSearchController : Controller
    {
        private readonly CreditSafeConfig _creditSafeConfig;
        private readonly ExchangeConfig _exchangeConfig;
        private readonly NrecoConfig _nrecoConfig;
        private readonly ILogger<DashroSafeSearchController> _logger;
        readonly int[] SearchTypes = new int[2] {0, 1};
        private string pdfFile { get; set; }

        private bool emailSent { get; set; }

        public DashroSafeSearchController(CreditSafeConfig creditSafeConfig, ExchangeConfig exchangeConfig, NrecoConfig nrecoConfig, ILogger<DashroSafeSearchController> logger)
        {
            _creditSafeConfig = creditSafeConfig;
            _exchangeConfig = exchangeConfig;
            _nrecoConfig = nrecoConfig;
            _logger = logger;
        }

        [HttpPost]
        public async Task<ActionResult> SafeSearch(Request request, [FromQuery] int searchType, [FromQuery] int clientId)
        {
            //string searchResult = string.Empty;
            var searchResult = new Dictionary<string, object>();

            //Create new instances of the SearchRepository.
            var searchRepository = new SearchRepository(_creditSafeConfig, _exchangeConfig, _nrecoConfig,_logger);

            //Create new instances of the GenericFunctionsRepository.
            var genericFunctionsRepository = new GenericFunctionsRepository(_creditSafeConfig, _nrecoConfig, _logger);

            //Create new instances of the SqlRepository.
            var sqlRepository = new SqlRepository(_creditSafeConfig, _logger);

            //Create new instance of the EmailRepository.
            var emailRepository = new EmailRepository(_creditSafeConfig, _exchangeConfig, _nrecoConfig, _logger);

            bool ftp = (bool)request.Ftp.HasValue ? (bool)request.Ftp : false;

            //Fetch the File Paths.
            var filePaths = await sqlRepository.FetchFilePaths();

            //Fetch the client details once validation has passed.
            ClientDetails clientDetails = await sqlRepository.FetchClientDetails(clientId);

            //Determine if the client can run any requests.
            if (!clientDetails.Active)
            {
                ResponseBody responseBody = new ResponseBody
                {
                    Message = "Unable to run any searches as the account is inactive."
                };

                return BadRequest(responseBody);
            }
            else if (clientDetails.SearchRequestsAvailable <= 0)
            {
                ResponseBody responseBody = new ResponseBody
                {
                    Message = $"You have used all your available searches for today. Searches allocated {clientDetails.SearchRequestsAllocated}, searches used {clientDetails.SearchRequestsUsed}."
                };

                return BadRequest(responseBody);
            }

            //Check to see if the end date has been reached.
            if(DateTime.UtcNow > clientDetails.EndDate)
            {
                ResponseBody responseBody = new ResponseBody
                {
                    Message = $"Your end date ({String.Format("{0:dd/MM/yyyy}", clientDetails.EndDate)}) for running searches has been reached."
                };
                return BadRequest(responseBody);
            }

            //Assign the Search Type Id to the variable searchTypeId.
            var searchTypeId = searchType;
            if (!SearchTypes.Contains(searchTypeId))
            {
                return BadRequest($"{searchTypeId} is an invalid Search TypeId, this must be 0 or 1.");
            };

            //Fetch a list of valid currencies to validate against.
            var countries = await sqlRepository.FetchCountries();
            if (countries == null)
            {
                string msg = "Error retrieving country details from the database.";
                _logger.LogInformation(msg);
                throw new Exception(msg);
            }

            //Validate the Payload.
            var isValid = Validate(request, countries, searchTypeId, clientId);
            if (!string.IsNullOrEmpty(isValid))
            {
                _logger.LogError("Validation error occurred - Message {}. ClientId={ClientId}", isValid, clientId);
                return BadRequest(isValid);
            }

            //Set the default threshold at 85.
            if (request.Threshold < 85)
            {
                request.Threshold = 85;
            }

            //Save the payload details to the logging table.
            _logger.LogInformation("Payload submitted ClientId={ClientId} | {}", clientId, JSON.Serializer(request));

            //Fetch the Search Person record based on the SearchType value 1, 2 or 3.
            switch (searchTypeId)
            {
                case 0:
                    PersonSearch personSearch = new PersonSearch
                    {
                        threshold = request.Threshold,
                        pep = request.Pep,
                        previousSanctions = request.PreviousSanctions,
                        currentSanctions = request.CurrentSanctions,
                        lawEnforcement = request.LawEnforcement,
                        financialRegulator = request.FinancialRegulator,
                        insolvency = request.Insolvency,
                        disqualifiedDirector = request.DisqualifiedDirector,
                        adverseMedia = request.AdverseMedia,
                        forename = request.Forename,
                        middlename = request.Middlename,
                        surname = request.Surname,
                        dateOfBirth = request.DateOfBirth,
                        yearOfBirth = request.YearOfBirth,
                        address = request.Address,
                        city = request.City,
                        county =request.County,
                        postCode = request.PostCode,
                        country = request.Country
                    };
                    searchResult = await searchRepository.PersonSearch(personSearch, clientDetails, !string.IsNullOrEmpty(request.Email), ftp);
                    var successPerson = genericFunctionsRepository.CheckKeyExists(searchResult, "Message");
                    clientDetails.Type = "Person Search";

                    //Construct the email body if there are no errors found.
                    if ((!successPerson && !string.IsNullOrEmpty(request.Email)) || ftp)
                    {
                        //Construct the HTML string.
                        var htmlString = searchRepository.htmlString;

                        //Create the PDF File
                        pdfFile = await genericFunctionsRepository.GeneratePdfFile(RequestType.Individual, clientId, 
                            filePaths.Find(f => f.PathName == "SafeSearch").PathFilePath, htmlString, clientDetails.Name, ftp, $"{request.Forename} {request.Surname}");

                        //Assign the remainder of the  Client details.
                        clientDetails.FilePath = pdfFile;
                        clientDetails.FileName = Path.GetFileName(pdfFile);
                        clientDetails.Email = !string.IsNullOrEmpty(request.Email) ? request.Email : clientDetails.Email;
                    }

                    if (!ftp)
                    {
                        //Check to see if an email is required and that an Html file exists.
                        if ((!string.IsNullOrEmpty(request.Email) && !string.IsNullOrEmpty(pdfFile)))
                        {
                            var searchName = string.IsNullOrEmpty(request.BusinessName) ? $"{request.Forename} {(string.IsNullOrEmpty(request.Middlename) ? "" : request.Middlename)} {request.Surname}" : request.BusinessName;
                            emailSent = await emailRepository.SendEmail(clientDetails, request, searchName);
                        }
                    }
                    break;
                default:
                    BusinessSearch businessSearch = new BusinessSearch
                    {
                        threshold = request.Threshold,
                        BusinessName = request.BusinessName, 
                        pep = request.Pep,
                        previousSanctions =  request.PreviousSanctions,
                        currentSanctions = request.CurrentSanctions,
                        lawEnforcement = request.LawEnforcement,
                        financialRegulator = request.FinancialRegulator,
                        insolvency = request.Insolvency,
                        disqualifiedDirector = request.DisqualifiedDirector,
                        adverseMedia = request.AdverseMedia,
                        address = request.Address,
                        city = request.City,
                        county = request.County,
                        postCode = request.PostCode,
                        country = request.Country
                    };
                    
                    searchResult = await searchRepository.BusinessSearch(businessSearch, clientDetails, string.IsNullOrEmpty(request.Email) ? false : true, ftp, searchTypeId);
                    var successBusiness = genericFunctionsRepository.CheckKeyExists(searchResult, "Message");
                    clientDetails.Type ="Business Search";

                    //Construct the email body.
                    if ((!successBusiness && !string.IsNullOrEmpty(request.Email)) || ftp)
                    {
                        //Construct the HTML string.
                        var htmlString = searchRepository.htmlString;

                        //Create the PDF File
                        pdfFile = await genericFunctionsRepository.GeneratePdfFile(searchTypeId == 1 ? RequestType.Business : RequestType.Bank, 
                            clientId, filePaths.Find(f => f.PathName == "SafeSearch").PathFilePath, htmlString, clientDetails.Name, ftp, $"{request.BusinessName}");

                        //Assign the remainder of the  Client details.
                        clientDetails.FilePath = pdfFile;
                        clientDetails.FileName = Path.GetFileName(pdfFile);
                        clientDetails.TypeId = searchType;
                        clientDetails.Email = !string.IsNullOrEmpty(request.Email) ? request.Email : clientDetails.Email;

                        if (!ftp)
                        {
                            //Check to see if an email is required and that an Html file exists.
                            if ((!string.IsNullOrEmpty(request.Email) && !string.IsNullOrEmpty(pdfFile)))
                            {
                                var searchName = string.IsNullOrEmpty(request.BusinessName) ? $"{request.Forename} {(string.IsNullOrEmpty(request.Middlename) ? "" : request.Middlename)} {request.Surname}" : request.BusinessName;
                                emailSent = await emailRepository.SendEmail(clientDetails, request, searchName);
                            }
                        }
                    }
                    break;
            }

            //If no response then return the message, this will exist when an error is raised or no records are found.
            if(!genericFunctionsRepository.CheckKeyExists(searchResult,"Message"))
            {
                var result = JSON.Deserializer(JSON.Serializer(searchResult["Response"]));
                return Ok(result);
            }
            else
            {
                var result = searchResult["Message"];
                var searchName = string.IsNullOrEmpty(request.BusinessName) ? $"{request.Forename} {(string.IsNullOrEmpty(request.Middlename) ? "" : request.Middlename)} {request.Surname}" : request.BusinessName;
                if (!ftp)
                {
                    var emailSent = emailRepository.SendNoResultsEmail(clientDetails, request, searchName);
                }

                return BadRequest(result);
            }
        }

        /// <summary>
        /// Perform validation on the request payload.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        private string Validate(Request request, List<Countries> countries, int requestType, double clientId)
        {
            if (request.Threshold <= 0 | request.Threshold > 100)
            {
               var  msg = $"Threshhold value {request.Threshold} should be between 1 and 100";
                return msg;
            }

            if (clientId <= 0 || !clientId.ToString().All(char.IsNumber))
            {
                var msg = $"{((clientId <= 0) ? "Client Id cannot be 0." : "Client Id must contain numeric values only.")}";
                return msg;
            }

            if(!string.IsNullOrEmpty(request.Country))
            {
                if(!countries.Exists(c=> c.Country == request.Country))
                {
                    var msg = $"Invalid country value {request.Country} used.";
                    return msg;
                }
            }

            if (!string.IsNullOrEmpty(request.Email))
            {
                string[] emails = request.Email.Split(";");
                foreach (string email in emails)
                { 
                if (!new EmailAddressAttribute().IsValid(email))
                {
                    var msg = $"Invalid email address {email} used.";
                    return msg;
                }
            }
            }

            //Person specific validation.
            if (requestType==0)
                {

                if (string.IsNullOrEmpty(request.Surname))
                {
                    var msg = "Surname is a required field for a person Search!";
                    return msg;
                }

                if(request.DateOfBirth !=null)
                {
                    if (DateTime.TryParse(request.DateOfBirth, out DateTime tempTo) == false)
                    {
                        var msg = "Invalid format for the field Date of Birth!";
                        return msg;
                    }
                }

                if (!string.IsNullOrEmpty(request.YearOfBirth))
                {
                    if (!request.YearOfBirth.ToString().All(char.IsNumber))
                    {
                        var msg = $"Year of birth {request.YearOfBirth} must be a numeric value.";
                        return msg;
                    }
                }

            }
            else //Business specific validation.
            {
                if (string.IsNullOrEmpty(request.BusinessName))
                {
                    var msg = "Business name is a required field for a business Search!";
                    return msg;
                }
            }

            return string.Empty;
        }
        private static bool IsNumericFromTryParse(string value)
        {
            double result = 0;
            return (double.TryParse(value, System.Globalization.NumberStyles.Float,
                    System.Globalization.NumberFormatInfo.CurrentInfo, out result));
        }
    }
}
