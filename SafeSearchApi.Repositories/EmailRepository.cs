﻿using SafeSearchApi.Models.Api;
using SafeSearchApi.Models.Configuration;
using SafeSearchApi.Repositories.Interfaces;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;
using Microsoft.Exchange.WebServices.Data;
using Microsoft.Identity.Client;
using System.Collections.Generic;

namespace SafeSearchApi.Repositories
{
    public class EmailRepository : IEmail
    {
        private readonly CreditSafeConfig _creditSafeConfig;
        private readonly ExchangeConfig _exchangeConfig;
        private readonly NrecoConfig _nrecoConfig;
        private readonly ILogger _logger;
        private string htmlString { get; set; }
        public string completeHtmlString { get; set; }

        public EmailRepository(CreditSafeConfig creditSafeConfig, ExchangeConfig exchangeConfig, NrecoConfig nrecoConfig, ILogger logger)
        {
            _creditSafeConfig = creditSafeConfig;
            _exchangeConfig = exchangeConfig;
            _nrecoConfig = nrecoConfig;
            _logger = logger;
        }

        private async Task<ExchangeService> getService()
        {
            var identityToken = ConfidentialClientApplicationBuilder
                    .Create(_exchangeConfig.AppId)
                    .WithClientSecret(_exchangeConfig.ClientSecret)
                    .WithTenantId(_exchangeConfig.TenantId)
                    .Build();

            var scopes = new string[] { "https://outlook.office365.com/.default" };
            var token = await identityToken.AcquireTokenForClient(scopes).ExecuteAsync();

            ExchangeService service = new ExchangeService(ExchangeVersion.Exchange2016)
            {
                TraceEnabled = true,
                UseDefaultCredentials = false,
                Credentials = new OAuthCredentials(token.AccessToken),
                Url = new Uri(_exchangeConfig.ExchangeEmailPath)
            };

            return service;
        }

        private void BuildHtmlString(string title, string value, string htmlBody, bool titleHeader = false)
        {
            if (!string.IsNullOrEmpty(title))
            {
                htmlString += "<tr><td width=20% valign=top><span style ='font-size:medium;font-family:\"Segoe UI\",\"sans-serif\";color:#172B4D'>" + title + ":</span></td><td width=20% valign=top><span style ='font-size:10.0pt;font-family:\"Arial\",\"sans-serif\"'>" + value + "</span></td></tr>";
            }
            else
            {
                htmlString += "<tr><td width=20% valign=top><span style ='font-size:9.0pt;font-family:\"Segoe UI\",\"sans-serif\";color:#172B4D'>" + title + ":</span></td><td width=20% valign=top><span style ='font-size:10.0pt;font-family:\"Arial\",\"sans-serif\"'>" + value + "</span></td></tr>";
            }
        }

        public void FinishHtml()
        {
            completeHtmlString = "<head><meta charset=\"UTF-8\"/><table Class=Style1 border=1 cellspacing=0 cellpadding=0 align=left style='border-collapse:collapse;border:none;margin-left:6.75pt;margin-right:6.75pt'>" + htmlString + "</table></head>";
        }

        public void ConstructPersonEmail(PersonResponse personResponse, bool newRecord)
        {
            try
            {
                BuildHtmlString("New Person", "", "", newRecord);
                BuildHtmlString("score", personResponse.score.ToString(), "");
                BuildHtmlString("id", personResponse.person.id, "");
                BuildHtmlString("title", personResponse.person.title.description, "");
                BuildHtmlString("alternativeTitle", personResponse.person.alternativeTitle, "");
                BuildHtmlString("forename", personResponse.person.forename, "");
                BuildHtmlString("middlename", personResponse.person.middlename, "");
                BuildHtmlString("surname", personResponse.person.surname, "");
                BuildHtmlString("dateOfBirth", personResponse.person.dateOfBirth.ToString(), "");
                BuildHtmlString("yearOfBirth", personResponse.person.yearOfBirth, "");
                BuildHtmlString("dateOfDeath", personResponse.person.dateOfDeath.ToString(), "");
                BuildHtmlString("yearOfDeath", personResponse.person.yearOfDeath, "");
                BuildHtmlString("isDeceased", personResponse.person.isDeceased.ToString(), "");
                BuildHtmlString("gender", personResponse.person.gender, "");
                BuildHtmlString("nationality", personResponse.person.nationality.nationality, "");
                BuildHtmlString("imageURL", personResponse.person.imageURL, "");
                BuildHtmlString("telephoneNumber", personResponse.person.telephoneNumber, "");
                BuildHtmlString("faxNumber", personResponse.person.faxNumber, "");
                BuildHtmlString("mobileNumber", personResponse.person.mobileNumber, "");
                BuildHtmlString("email", personResponse.person.email, "");
                BuildHtmlString("pepLevel", personResponse.person.pepLevel, "");
                BuildHtmlString("isPEP", personResponse.person.isPEP.ToString(), "");
                BuildHtmlString("isSanctionsCurrent", personResponse.person.isSanctionsCurrent.ToString(), "");
                BuildHtmlString("isSanctionsPrevious", personResponse.person.isSanctionsPrevious.ToString(), "");
                BuildHtmlString("isLawEnforcement", personResponse.person.isLawEnforcement.ToString(), "");
                BuildHtmlString("isFinancialregulator", personResponse.person.isFinancialregulator.ToString(), "");
                BuildHtmlString("isDisqualifiedDirector", personResponse.person.isDisqualifiedDirector.ToString(), "");
                BuildHtmlString("isInsolvent", personResponse.person.isInsolvent.ToString(), "");
                BuildHtmlString("isAdverseMedia", personResponse.person.isAdverseMedia.ToString(), "");
                for (int i = 0; i < personResponse.person.addresses.Length; i++)
                {
                    BuildHtmlString("address1", personResponse.person.addresses[i].address1, "");
                    BuildHtmlString("address2", personResponse.person.addresses[i].address2, "");
                    BuildHtmlString("address3", personResponse.person.addresses[i].address3, "");
                    BuildHtmlString("address4", personResponse.person.addresses[i].address4, "");
                    BuildHtmlString("city", personResponse.person.addresses[i].city, "");
                    BuildHtmlString("county", personResponse.person.addresses[i].county, "");
                    BuildHtmlString("postcode", personResponse.person.addresses[i].postcode, "");
                    BuildHtmlString("country", personResponse.person.addresses[i].country.name, "");
                }
                for (int i = 0; i < personResponse.person.aliases.Length; i++)
                {
                    BuildHtmlString("title", personResponse.person.aliases[i].title, "");
                    BuildHtmlString("alternativeTitle", personResponse.person.aliases[i].alternativeTitle, "");
                    BuildHtmlString("forename", personResponse.person.aliases[i].forename, "");
                    BuildHtmlString("middlename", personResponse.person.aliases[i].middlename, "");
                    BuildHtmlString("surname", personResponse.person.aliases[i].surname, "");
                    BuildHtmlString("businessName", "n/a", "");
                }
                for (int i = 0; i < personResponse.person.articles.Length; i++)
                {
                    if (personResponse.person.articles[i].categories.Length > 0)
                    {
                        BuildHtmlString("originalURL", personResponse.person.articles[i].originalURL, "");
                        BuildHtmlString("dateCollected", personResponse.person.articles[i].dateCollected == null ? string.Empty : personResponse.person.articles[i].dateCollected.ToString(), "");
                        BuildHtmlString("c6URL", personResponse.person.articles[i].c6URL.ToString(), "");
                        for (int c = 0; c < (personResponse.person.articles[i].categories.Length > 3 ? 3 : personResponse.person.articles[i].categories.Length); c++)
                        {
                            BuildHtmlString($"ArticleCategory{i + 1}", personResponse.person.articles[i].categories[c].name, "");
                        }
                    }
                }
                for (int i = 0; i < personResponse.person.sanctions.Length; i++)
                {
                    BuildHtmlString("sanctionType", personResponse.person.sanctions[i].sanctionType.description, "");
                    BuildHtmlString("isCurrent", personResponse.person.sanctions[i].isCurrent, "");
                }
                for (int i = 0; i < personResponse.person.notes.Length; i++)
                {
                    BuildHtmlString("dataSource", personResponse.person.notes[i].dataSource == null ? string.Empty : personResponse.person.notes[i].dataSource.name, "");
                    BuildHtmlString("text", personResponse.person.notes[i].text, "");
                }
                for (int i = 0; i < personResponse.person.linkedBusinesses.Length; i++)
                {
                    BuildHtmlString("businessName", personResponse.person.linkedBusinesses[i].businessName, "");
                    BuildHtmlString("position", personResponse.person.linkedBusinesses[i].position, "");
                }
                for (int i = 0; i < personResponse.person.linkedPersons.Length; i++)
                {
                    BuildHtmlString("personId", personResponse.person.linkedPersons[i].personId.ToString(), "");
                    BuildHtmlString("name", personResponse.person.linkedPersons[i].name, "");
                    BuildHtmlString("association", personResponse.person.linkedPersons[i].association, "");
                    BuildHtmlString("position", string.Empty, "");
                }
                for (int i = 0; i < personResponse.person.politicalPositions.Length; i++)
                {
                    BuildHtmlString("description", personResponse.person.politicalPositions[i].description, "");
                    BuildHtmlString("from", personResponse.person.politicalPositions[i].from == null ? string.Empty : personResponse.person.politicalPositions[i].from.ToString(), "");
                    BuildHtmlString("to", personResponse.person.politicalPositions[i].to ==null ? string.Empty : personResponse.person.politicalPositions[i].to.ToString(), "");
                    BuildHtmlString("country", personResponse.person.politicalPositions[i].country.name, "");
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message, "Error creating Person Search html");
            }
        }

        public void ConstructBusinessEmail(BusinessResponse businessResponse, bool newRecord)
        {
            try
            {
                BuildHtmlString("New business", "", "", newRecord);
                BuildHtmlString("score", businessResponse.score.ToString(), "");
                BuildHtmlString("id", businessResponse.business.id, "");
                BuildHtmlString("businessname", businessResponse.business.businessName, "");
                BuildHtmlString("telephoneNumber", businessResponse.business.telephoneNumber, "");
                BuildHtmlString("faxNumber", businessResponse.business.faxNumber, "");
                BuildHtmlString("website", businessResponse.business.website, "");
                BuildHtmlString("isPEP", businessResponse.business.isPEP.ToString(), "");
                BuildHtmlString("isSanctionsCurrent", businessResponse.business.isSanctionsCurrent.ToString(), "");
                BuildHtmlString("isSanctionsPrevious", businessResponse.business.isSanctionsPrevious.ToString(), "");
                BuildHtmlString("isLawEnforcement", businessResponse.business.isLawEnforcement.ToString(), "");
                BuildHtmlString("isFinancialregulator", businessResponse.business.isFinancialregulator.ToString(), "");
                BuildHtmlString("isDisqualifiedDirector", businessResponse.business.isDisqualifiedDirector.ToString(), "");
                BuildHtmlString("isInsolvent", businessResponse.business.isInsolvent.ToString(), "");
                BuildHtmlString("isAdverseMedia", businessResponse.business.isAdverseMedia.ToString(), "");
                for (int i = 0; i < businessResponse.business.addresses.Length; i++)
                {
                    BuildHtmlString("address1", businessResponse.business.addresses[i].address1, "");
                    BuildHtmlString("address2", businessResponse.business.addresses[i].address2, "");
                    BuildHtmlString("address3", businessResponse.business.addresses[i].address3, "");
                    BuildHtmlString("address4", businessResponse.business.addresses[i].address4, "");
                    BuildHtmlString("city", businessResponse.business.addresses[i].city, "");
                    BuildHtmlString("county", businessResponse.business.addresses[i].county, "");
                    BuildHtmlString("postcode", businessResponse.business.addresses[i].postcode, "");
                    BuildHtmlString("country", businessResponse.business.addresses[i].country.name, "");
                }
                for (int i = 0; i < businessResponse.business.aliases.Length; i++)
                {
                    BuildHtmlString("title", businessResponse.business.aliases[i].title, "");
                    BuildHtmlString("alternativeTitle", businessResponse.business.aliases[i].alternativeTitle, "");
                    BuildHtmlString("forename", businessResponse.business.aliases[i].forename, "");
                    BuildHtmlString("middlename", businessResponse.business.aliases[i].middlename, "");
                    BuildHtmlString("surname", businessResponse.business.aliases[i].surname, "");
                    BuildHtmlString("businessName", businessResponse.business.aliases[i].businessName, "");
                }
                for (int i = 0; i < businessResponse.business.articles.Length; i++)
                {
                    if (businessResponse.business.articles[i].categories.Length > 0)
                    {
                        BuildHtmlString("originalURL", businessResponse.business.articles[i].originalURL, "");
                        BuildHtmlString("dateCollected", businessResponse.business.articles[i].dateCollected==null? string.Empty : businessResponse.business.articles[i].dateCollected.ToString(), "");
                        BuildHtmlString("c6URL", businessResponse.business.articles[i].c6URL.ToString(), "");
                        for (int c = 0; c < (businessResponse.business.articles[i].categories.Length > 3 ? 3 : businessResponse.business.articles[i].categories.Length); c++)
                        {
                            BuildHtmlString($"ArticleCategory{i + 1}", businessResponse.business.articles[i].categories[c].name, "");
                        }
                    }
                }
                for (int i = 0; i < businessResponse.business.sanctions.Length; i++)
                {
                    BuildHtmlString("sanctionType", businessResponse.business.sanctions[i].sanctionType.description, "");
                    BuildHtmlString("isCurrent", businessResponse.business.sanctions[i].isCurrent, "");
                }
                for (int i = 0; i < businessResponse.business.notes.Length; i++)
                {
                    BuildHtmlString("dataSource", businessResponse.business.notes[i].dataSource == null ? string.Empty : businessResponse.business.notes[i].dataSource.name, "");
                    BuildHtmlString("text", businessResponse.business.notes[i].text, "");
                }
                for (int i = 0; i < businessResponse.business.linkedBusinesses.Length; i++)
                {
                    BuildHtmlString("businessName", businessResponse.business.linkedBusinesses[i].businessName, "");
                    BuildHtmlString("linkDescription", businessResponse.business.linkedBusinesses[i].linkDescription, "");
                }
                for (int i = 0; i < businessResponse.business.linkedPersons.Length; i++)
                {
                    BuildHtmlString("personId", businessResponse.business.linkedPersons[i].personId.ToString(), "");
                    BuildHtmlString("name", businessResponse.business.linkedPersons[i].name, "");
                    BuildHtmlString("association", string.Empty, "");
                    BuildHtmlString("position", businessResponse.business.linkedPersons[i].position, "");
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message, "Error creating business Search html.");
            }
        }

        public async Task<bool> SendEmail(ClientDetails clientDetails, Request request, string searchName) 
        {

            try
            {
                var htmlBody = "<!DOCTYPE html><style>";
                htmlBody += "div {font-size: 16px;font-family: Tahoma;color: #666666; padding-left:12pt;},";
                htmlBody += "table.datatable{border: 1px solid; border-color:#000033;font-size: 14px;font-family: Tahoma; border-collapse: collapse;},";
                htmlBody += "td.datadef{border: 1px solid; padding-left:10pt; font-family:Calibri; font-size: 16px; padding-right:10pt; color:#193366; height:25px;},";
                htmlBody += "th.datahd{border: 1px solid; vertical-align:center; font-family:Calibri; font-size: 18px; color: #193366; width:45%}";
                htmlBody += "</style>";
                htmlBody += $"<div><p>The {clientDetails.Type} search report on {searchName} is attached to this email.</p></div>";
                htmlBody += "<div><p>The search request parameters are shown below for your reference.</p></div>";
                htmlBody += "<div><table class=\"datatable\"><th class=\"datahd\">Request Parameter</th><th class=\"datahd\">Request Value</th>";

                var jsonRequest = JSON.Serializer(request);

                Dictionary<string, string> matchItems = (Dictionary<string, string>)JSON.Deserializer<Dictionary<string, string>>(jsonRequest);

                foreach (KeyValuePair<string, string> entry in matchItems)
                {
                    htmlBody += $"<tr><td class=\"datadef\">{entry.Key}</td><td class=\"datadef\">{entry.Value}</td></tr>";
                }
                htmlBody += "</table></div>";

                ExchangeService service = await getService();
                service.ImpersonatedUserId = new ImpersonatedUserId(ConnectingIdType.SmtpAddress, _exchangeConfig.ExchangeAddress);
                service.HttpHeaders.Add("X-AnchorMailbox", _exchangeConfig.ExchangeAddress);

                var email = new EmailMessage(service)
                {
                    Subject = $"{clientDetails.Type} Report for {searchName} requested by {clientDetails.Name}."
                };
                email.Attachments.AddFileAttachment(clientDetails.FileName, clientDetails.FilePath);
                email.Body = new MessageBody(BodyType.HTML, htmlBody);
                string[] emailAddresses = clientDetails.Email.Split(";");
                foreach(string emailaddress in emailAddresses)
                {
                    email.ToRecipients.Add(emailaddress);
                }
                email.BccRecipients.Add(_creditSafeConfig.BccAddress);
                await email.SendAndSaveCopy();

                _logger.LogInformation("Email successfully sent to {} containing the file {}. ClientId={ClientId}", clientDetails.Email, clientDetails.FilePath, clientDetails.ClientId);

                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Unable to send the email to {} containing the file {}. ClientId={ClientId}", clientDetails.Email, clientDetails.FilePath, clientDetails.ClientId);
            }
            return false;
        }

        public async Task<bool> SendAuthenticationEmail(ClientRequest clientRequest, ClientResponse clientResponse, string authKey) 
        {
            try
            {
                var validauthKey = authKey.Replace("+", "%2B");
                //string url = $"{"https://localhost:44336/api/v1/Client/Authenticate?"}authKey={validauthKey}";
                string url = $"{_creditSafeConfig.SwiftApiUrl}authKey={validauthKey}";

                var htmlBody = "<!DOCTYPE html lang=\"en\" xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:o=\"urn:schemas-microsoft-com:office:office\"><html><style>";
                htmlBody += "td.fmttextbig {display: block; text-align: center; font-family: Arial, Helvetica, sans-serif, Tahoma; color:#666666; font-size:36px; font-weight:900; padding-top:20px;}";
                htmlBody += "td.fmttext {display: block; text-align: center; font-family: Arial, Helvetica, sans-serif, Tahoma; color:#666666; font-size:17px; margin-left:20px; margin-right:20px; padding:10px 0px 10px 0px}";
                htmlBody += "span {font-family: Arial, Helvetica, sans-serif, Tahoma; font-size:18px; vertical-align: middle;}";
                htmlBody += "a:visited {color:#FFFFFF; text-align:center;}";
                htmlBody += "td {mso-table-lspace: 0pt !important;mso-table-rspace: 0pt !important;}";
                htmlBody += "td.fmtftr {text-align: center; font-family: Arial, Helvetica, sans-serif, Tahoma; color:#666666; font-size:12px;}";
                htmlBody += "td.fmttbl2 {text-align: center; height: 30px; background-color:#FDF5E6; font-family: Arial, Helvetica, sans-serif, Tahoma; color:#666666; font-size:12px; border-width: 1px; border-style: solid; border-color: #C1c1c1;}";
                htmlBody += "</style><head></head><!--[if (gte mso 9)|(IE)]><style type=\"text/css\">table {border-collapse: collapse;border-spacing: 0;mso-table-lspace: 0pt !important;mso-table-rspace: 0pt !important; }</style><![endif]-->";
                htmlBody += "<body style=\"width:900px; margin: 0 auto !important; padding: 0 !important; margin-top:50px;\">";
                htmlBody += "<form method=\"GET\">";
                htmlBody += "<table style=\"margin-left:100px; width:700px; height:450px; background-color: #E4E4E4; perspective:1px; separate !important;\" role=\"presentation\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">";
                htmlBody += "<tr><td style=\"padding:20px 20px 0px 20px;text-align:center;\">";
                htmlBody += "<img src=\"C:\\EmailImages\\DRS_Big.png\" style =\"display: block; text-align: center; margin: auto; vertical-align:middle; border-width: 1px; border-style: solid; border-color: #C1c1c1;\" alt=\"img\"></td></tr>";
                htmlBody += "<tr><td style=\"display:block; text-align:center; font-family: Arial, Helvetica, sans-serif, Tahoma; color:#666666; font-size:36px; font-weight:900; padding-top:20px;\">Activate Your Account</td>"; //Need to replace source!
                htmlBody += "</tr><tr><td class=\"fmttext\"><p>You’ve received this message because you registered your email with Dashro Solutions on our site. Please click the button below to verify your email address and create your new account.</p>";
                htmlBody += "<p>This link will expire in 30 minutes, at which point you will need to request a new authentication email from the client management page.</p>";
                htmlBody += "</td></tr><tr>";
                htmlBody += "<td style=\"text-align:center; margin-bottom:5px; border-color: #2cb543;\">";
                htmlBody += $"<a href=\"{url}\" style=\"background-color:#c8733f; text-align:center; font-size:18px; font-family:Helvetica, Arial, sans-serif; font-weight:bold; text-decoration:none; color:inherit;color:#ffffff;\">";
                htmlBody += "<!--[if mso]><i style=\"letter-spacing:0px; mso-text-raise:30pt;\">&nbsp;</i><![endif]-->";
                htmlBody += "<span style=\"mso-text-raise:15pt; text-align:left; vertical-align:middle;\">Click to activate your account</span><!--[if mso]><i style=\"letter spacing:0px;\">&nbsp;</i><![endif]--></a></td></tr>";
                htmlBody += "<tr><td class=\"fmttext\"><p>If you did not register with us, please disregard this email.</p></td></tr>";
                htmlBody += "<tr><td class=\"fmttbl2\"><p>© Dashro Solutions 2022 - Office 7, 35-37 Ludgate Hill, London EC4M 7JN</p></td></tr></table></form></body></html>";

                ExchangeService service = await getService();

                service.ImpersonatedUserId = new ImpersonatedUserId(ConnectingIdType.SmtpAddress, _exchangeConfig.ExchangeAddress);
                service.HttpHeaders.Add("X-AnchorMailbox", _exchangeConfig.ExchangeAddress);
                var email = new EmailMessage(service)
                {
                    Subject = $"Confirmation of new client account for {clientRequest.Forename} {(string.IsNullOrEmpty(clientRequest.Middlename) ? "" : clientRequest.Middlename)} {clientRequest.Surname}."
                };

                //email.Attachments.AddFileAttachment("C:\\EmailImages\\DRS_Big.png");
                email.Body = new MessageBody(BodyType.HTML, htmlBody);
                email.ToRecipients.Add(clientRequest.Email);
                email.BccRecipients.Add(_creditSafeConfig.BccAddress);
                await email.SendAndSaveCopy();

                _logger.LogInformation("Activation email successfully sent to {}.  ClientId={ClientId}", clientRequest.Email, clientResponse.ClientId);

                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Unable to send the activation email to {}.   ClientId={ClientId}", clientRequest.Email, clientResponse.ClientId);
            }
            return false; 
        }

        public async Task<bool> SendAuthenticationEmail(ClientDetails clientDetails, string authKey) 
        {
            try
            {
                var validauthKey = authKey.Replace("+", "%2B");
                //string url = $"{"https://localhost:44336/api/v1/Client/Authenticate?"}authKey={validauthKey}";
                string url = $"{_creditSafeConfig.SwiftApiUrl}authKey={validauthKey}";

                var htmlBody = "<!DOCTYPE html lang=\"en\" xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:o=\"urn:schemas-microsoft-com:office:office\"><html><style>";
                htmlBody += "td.fmttextbig {display: block; text-align: center; font-family: Arial, Helvetica, sans-serif, Tahoma; color:#666666; font-size:36px; font-weight:900; padding-top:20px;}";
                htmlBody += "td.fmttext {display: block; text-align: center; font-family: Arial, Helvetica, sans-serif, Tahoma; color:#666666; font-size:17px; margin-left:20px; margin-right:20px; padding:10px 0px 10px 0px}";
                htmlBody += "span {font-family: Arial, Helvetica, sans-serif, Tahoma; font-size:18px; vertical-align: middle;}";
                htmlBody += "a:visited {color:#FFFFFF; text-align:center;}";
                htmlBody += ".es-button {mso-style-priority:100!important;text-decoration:none!important; ";
                htmlBody += "td {mso-table-lspace: 0pt !important;mso-table-rspace: 0pt !important;}";
                htmlBody += "td.fmtftr {text-align: center; font-family: Arial, Helvetica, sans-serif, Tahoma; color:#666666; font-size:12px;}";
                htmlBody += "td.fmttbl2 {text-align: center; height: 30px; background-color:#FDF5E6; font-family: Arial, Helvetica, sans-serif, Tahoma; color:#666666; font-size:12px; border-width: 1px; border-style: solid; border-color: #C1c1c1;}";
                htmlBody += "</style><head></head><!--[if (gte mso 9)|(IE)]><style type=\"text/css\">table {border-collapse: collapse;border-spacing: 0;mso-table-lspace: 0pt !important;mso-table-rspace: 0pt !important; }</style><![endif]-->";
                htmlBody += "<body style=\"width:900px; margin: 0 auto !important; padding: 0 !important; margin-top:50px;\">";
                htmlBody += "<form method=\"GET\">";
                htmlBody += "<table style=\"margin-left:100px; width:700px; height:450px; background-color: #E4E4E4; perspective:1px; separate !important;\" role=\"presentation\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">";
                htmlBody += "<tr><td style=\"padding:20px 20px 0px 20px;text-align:center;\">";
                htmlBody += "<img src=\"C:\\EmailImages\\DRS_Big.png\" style=\"display: block; text-align: center; margin: auto; vertical-align:middle; border-width: 1px; border-style: solid; border-color: #C1c1c1;\" alt=\"img\"></td></tr>";
                htmlBody += "<tr><td style=\"display:block; text-align:center; font-family: Arial, Helvetica, sans-serif, Tahoma; color:#666666; font-size:36px; font-weight:900; padding-top:20px;\">Activate Your Account</td>"; //Need to replace source!
                htmlBody += "</tr><tr><td class=\"fmttext\"><p>You’ve received this message because you registered your email with Dashro Solutions on our site. Please click the button below to verify your email address and create your new account.</p>";
                htmlBody += "<p>This link will expire in 30 minutes, at which point you will need to request a new authentication email from the client management page.</p>";
                htmlBody += "</td></tr><tr><td style=\"text-align:center; margin-bottom:5px; border-color: #2cb543;\">";
                htmlBody += $"<a href=\"{url}\" class=\"es-button\" style=\"background-color:#c8733f; text-align:center; font-size:18px; font-family:Helvetica, Arial, sans-serif; font-weight:bold; text-decoration:none; color:inherit;color:#ffffff;\">";
                htmlBody += "<!--[if mso]><i style=\"letter-spacing:0px; mso-text-raise:30pt;\">&nbsp;</i><![endif]-->";
                htmlBody += "<span style=\"mso-text-raise:15pt; text-align:left; vertical-align:middle;\">Click to activate your account</span><!--[if mso]><i style=\"letter spacing:0px;\">&nbsp;</i><![endif]--></a></td></tr>";
                htmlBody += "<tr><td class=\"fmttext\"><p>If you did not register with us, please disregard this email.</p></td></tr>";
                htmlBody += "<tr><td class=\"fmttbl2\"><p>© Dashro Solutions 2022 - Office 7, 35-37 Ludgate Hill, London EC4M 7JN</p></td></tr></table></form></body></html>";

                ExchangeService service = await getService();

                service.ImpersonatedUserId = new ImpersonatedUserId(ConnectingIdType.SmtpAddress, _exchangeConfig.ExchangeAddress);
                service.HttpHeaders.Add("X-AnchorMailbox", _exchangeConfig.ExchangeAddress);
                var email = new EmailMessage(service)
                {
                    Subject = $"Confirmation of new client account for {clientDetails.Name}."
                };

                //email.Attachments.AddFileAttachment("C:\\EmailImages\\DRS_Big.png");
                email.Body = new MessageBody(BodyType.HTML, htmlBody);
                email.ToRecipients.Add(clientDetails.Email);
                email.BccRecipients.Add(_creditSafeConfig.BccAddress);
                await email.SendAndSaveCopy();

                _logger.LogInformation("Activation email successfully resent to {}.  ClientId={ClientId}", clientDetails.Email, clientDetails.ClientId);

                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Unable to resend the activation email to {}.   ClientId={ClientId}", clientDetails.Email, clientDetails.ClientId);
            }
            return false;
        }

        public async Task<bool> SendConfirmationEmail(ClientDetails clientDetails) 
        {
            try
            {
                var htmlBody = "<!DOCTYPE html lang=\"en\" xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:o=\"urn:schemas-microsoft-com:office:office\"><html><style>";
                htmlBody += "td.fmttext {text-align: center; font-family: Arial, Helvetica, sans-serif, Tahoma; color:#666666; font-size:17px; margin-bottom:50px; margin-left:20px; margin-right:20px; padding-bottom:20px;}";
                htmlBody += "span {font-family: Arial, Helvetica, sans-serif, Tahoma; font-size:17px; vertical-align: middle;text-decoration :italic;font-size: 16px;color:#ff3300;text-align: left;}";
                htmlBody += "a:visited {color:#FFFFFF; text-align:center;}";
                htmlBody += "td.clientleft {font-family: Arial, Helvetica, sans-serif, Tahoma; font-size:15px; color:#666666; padding-left:50px; font-weight:700}";
                htmlBody += "td.clientright {font-family: Arial, Helvetica, sans-serif, Tahoma; font-size:15px; color:#666666; padding-right:50px;}";
                htmlBody += "td.fmttbl2 {text-align: center; height: 30px; background-color:#FDF5E6; font-family: Arial, Helvetica, sans-serif, Tahoma; color:#666666; font-size:12px; border-width: 1px; border-style: solid; border-color: #C1c1c1;}";
                htmlBody += "</style><head></head><!--[if (gte mso 9)|(IE)]><style type=\"text/css\">table {border-collapse: collapse;border-spacing: 0;mso-table-lspace: 0pt !important;mso-table-rspace: 0pt !important; }</style><![endif]-->";
                htmlBody += "<body style=\"width:900px; margin: 0 auto!important; padding: 0!important; padding-top:60px;overflow-x:hidden;\">";
                htmlBody += "<table style=\"margin-left:100px; width:650px; height:350px; background-color: #E4E4E4; perspective: 1px; separate !important;\" role=\"presentation\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">";
                htmlBody += "<tr colspan=\"2\">";
                htmlBody += "<td colspan=\"2\" style=\"padding: 20px 20px 0px 20px; text-align: center;\">";
                htmlBody += "<img src=\"cid:DRS_Big.png\" style=\"display: block; text-align: center; margin: auto; vertical-align:middle;\" alt=\"img\"></td></tr>";
                htmlBody += "<tr colspan=\"2\">";
                htmlBody += "<td colspan=\"2\" style=\"text-align:center; font-family: Arial, Helvetica, sans-serif, Tahoma; color:#666666; font-size:36px; font-weight:900; padding-top:20px; padding-bottom:20px;\">";
                htmlBody += "Your Account Details</td></tr>";
                htmlBody += "<tr colspan=\"2\">";
                htmlBody += "<td colspan=\"2\" class=\"fmttext\">";
                htmlBody += "<p>Your new client details are displayed below, you will require these to carry out a person, business search or Swift file creation.</p>";
                htmlBody += $"<p>You are permitted to perform {clientDetails.SearchRequestsAllocated} searches per day and create  {clientDetails.SwiftRequestsAllocated} Swift files per day.</p>";
                htmlBody += "<p>If you wish to increase this number you can request additional items by going to the client area at ";
                htmlBody += "<a href=\"https://www.DashroSolutions.com\" style=\"text-decoration:none; color:inherit; color:#c8733f;\">Dashro Solutions</a></p>";
                htmlBody += "</td></tr><tr colspan=\"2\">";
                htmlBody += "<td class=\"clientleft\">Client Id:</td>";
                htmlBody += $"<td class=\"clientright\"><span><strong>{clientDetails.ClientId}</span></strong></td>";
                htmlBody += "</tr><tr colspan=\"2\">";
                htmlBody += "<td class=\"clientleft\">Api Key:</td>";
                htmlBody += $"<td class=\"clientright\"><span><strong>{clientDetails.ApiKey}</span></strong></td>";
                htmlBody += "</tr><tr colspan=\"2\">";
                htmlBody += "<td class=\"clientleft\">Password:</td>";
                htmlBody += $"<td class=\"clientright\"><span><strong>{clientDetails.Password}</span></strong></td>";
                htmlBody += "</tr><tr colspan=\"2\"><td colspan=\"2\">&nbsp;</td></tr><tr>";
                htmlBody += "<td class=\"fmttbl2\" colspan=\"2\">";
                htmlBody += "<p>© Dashro Solutions 2022 - Office 7, 35-37 Ludgate Hill, London EC4M 7JN</p>";
                htmlBody += "</td></tr></table></body></html>";

                ExchangeService service = await getService();

                service.ImpersonatedUserId = new ImpersonatedUserId(ConnectingIdType.SmtpAddress, _exchangeConfig.ExchangeAddress);
                service.HttpHeaders.Add("X-AnchorMailbox", _exchangeConfig.ExchangeAddress);
                var email = new EmailMessage(service)
                {
                    Subject = $"Client account details for {clientDetails.Name}."
                };

                //email.Attachments.AddFileAttachment("C:\\EmailImages\\DRS_Big.png");
                email.Body = new MessageBody(BodyType.HTML, htmlBody);
                email.ToRecipients.Add(clientDetails.Email);
                email.BccRecipients.Add(_creditSafeConfig.BccAddress);
                await email.SendAndSaveCopy();

                _logger.LogInformation("Confirmation email successfully sent to {}.  ClientId={ClientId}", clientDetails.Email, clientDetails.ClientId);

                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Unable to send the Confirmation email to {}.   ClientId={ClientId}", clientDetails.Email, clientDetails.ClientId);
            }
            return false;
        }

        public async Task<bool> SendNoResultsEmail(ClientDetails clientDetails, Request request, string searchName) 
        {
            try
            {
                var htmlBody = "<!DOCTYPE html><style>";
                htmlBody += "div {font-size: 16px;font-family: Tahoma;color: #666666; padding-left:12pt;},";
                htmlBody += "table.datatable{border: 1px solid; border-color:#000033;font-size: 14px;font-family: Tahoma; border-collapse: collapse;},";
                htmlBody += "td.datadef{border: 1px solid; padding-left:10pt; font-family:Calibri; font-size: 16px; padding-right:10pt; color:#193366; height:25px;},";
                htmlBody += "th.datahd{border: 1px solid; vertical-align:center; font-family:Calibri; font-size: 18px; color: #193366;}";
                htmlBody += "</style>";
                htmlBody += $"<div><p>No records were returned for the {clientDetails.Type} search requested on {searchName}.</p></div>";
                htmlBody += "<div><p>The search request parameters are shown below for your reference.</p></div>";
                htmlBody += "<div><table class=\"datatable\"><th class=\"datahd\">Request Parameter</th><th class=\"datahd\">Request Value</th>";

                var jsonRequest = JSON.Serializer(request);

                Dictionary<string, string> matchItems = (Dictionary<string, string>)JSON.Deserializer<Dictionary<string, string>>(jsonRequest);

                foreach (KeyValuePair<string, string> entry in matchItems)
                {
                    htmlBody += $"<tr><td class=\"datadef\">{entry.Key}</td><td class=\"datadef\">{entry.Value}</td></tr>";
                }
                htmlBody +="</table></div>";
               ExchangeService service = await getService();

                service.ImpersonatedUserId = new ImpersonatedUserId(ConnectingIdType.SmtpAddress, _exchangeConfig.ExchangeAddress);
                service.HttpHeaders.Add("X-AnchorMailbox", _exchangeConfig.ExchangeAddress);
                var email = new EmailMessage(service)
                {
                    Subject = $"No records returned for {clientDetails.Type} Report for {searchName} requested by {clientDetails.Name}."
                };

                email.Body = new MessageBody(BodyType.HTML, htmlBody);
                string[] emailAddresses = string.IsNullOrEmpty(request.Email) ? clientDetails.Email.Split(";") : request.Email.Split(";");
                foreach (string emailaddress in emailAddresses)
                {
                    email.ToRecipients.Add(emailaddress);
                }
                email.BccRecipients.Add(_creditSafeConfig.BccAddress);
                await email.SendAndSaveCopy();

                _logger.LogInformation("No records found email successfully resent to {}.  ClientId={ClientId}", clientDetails.Email, clientDetails.ClientId);

                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Unable to send the no records found email to {}.   ClientId={ClientId}", clientDetails.Email, clientDetails.ClientId);
            }
            return false;
        }
    }
}
