﻿using SafeSearchApi.Models.Api;
using SafeSearchApi.Models.Configuration;
using SafeSearchApi.Models.Enumerated_Types;
using SafeSearchApi.Repositories.Interfaces;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace SafeSearchApi.Repositories
{
    public class SearchRepository : ISearch
    {
        private readonly CreditSafeConfig _creditSafeConfig;
        private readonly ExchangeConfig _exchangeConfig;
        private readonly NrecoConfig _nrecoConfig;
        private readonly ILogger _logger;
        public static HttpResponseMessage responseMessage;
        IList valArray;
        Object searchResponse;
        int noOfResponses = 0;
        public string htmlString { get; set; }

        public SearchRepository(CreditSafeConfig creditSafeConfig, ExchangeConfig exchangeConfig, NrecoConfig nrecoConfig, ILogger logger)
        {
            _creditSafeConfig = creditSafeConfig;
            _exchangeConfig = exchangeConfig;
            _nrecoConfig = nrecoConfig;
            _logger = logger;
        }

        /// <summary>
        /// Perform the Persons Search.
        /// </summary>
        /// <param name="personSearch"></param>
        /// <param name="clientId"></param>
        /// <param name="emailAddress"></param>
        /// <returns></returns>
        public async Task<Dictionary<string, object>> PersonSearch(PersonSearch personSearch, ClientDetails clientDetails, bool emailAddress, bool ftp)
        {
            //Create new Dictionary Object to be returned in the method.
            var dictPerson = new Dictionary<string, object>();
            bool updateTables = false;
            bool newRecord = true;

            //Define the new Data Table instances.
            DataTable tblPerson = new DataTable();
            DataTable tblAddresses = new DataTable();
            DataTable tblAlias = new DataTable();
            DataTable tblArticles = new DataTable();
            DataTable tblSanctions = new DataTable();
            DataTable tblNotes = new DataTable();
            DataTable tblLinkedBusinesses = new DataTable();
            DataTable tblLinkedPersons = new DataTable();
            DataTable tblPoliticalPositions = new DataTable();

            //Instantiate a new instance of the class TablesRepository.
            TablesRepository tablesRepository = new TablesRepository(_creditSafeConfig, _logger);

            //Instantiate a new instance of the class SqlRepository.
            SqlRepository sqlRepository = new SqlRepository(_creditSafeConfig, _logger);

            //Instantiate a new instance of the class GenericFunctions.
            GenericFunctionsRepository genericFunctions = new GenericFunctionsRepository(_creditSafeConfig, _nrecoConfig, _logger);

            //Instantiate a new instance of the EmailRepository
            var emailRepository = new EmailRepository(_creditSafeConfig, _exchangeConfig, _nrecoConfig,_logger);

            try
            {
                var result = await RequestScorePayment(_creditSafeConfig.PersonUrl, personSearch, clientDetails, RequestType.Individual, _creditSafeConfig.PersonApiKey);

                if (result.ValidResponse && result.JsonResponse != "")
                {
                    _logger.LogInformation("{} response received from Person Search. ClientId={ClientId}", result.ResponseCode, clientDetails.ClientId);

                    //Populate the Data Tables.
                    tablesRepository.CreatePersonColumns(tblPerson);
                    tablesRepository.CreateAddressColumns(tblAddresses);
                    tablesRepository.CreateAliasColumns(tblAlias);
                    tablesRepository.CreateArticlesColumns(tblArticles);
                    tablesRepository.CreateSanctionsColumns(tblSanctions);
                    tablesRepository.CreateNotesColumns(tblNotes);
                    tablesRepository.CreateLinkedBusinessesColumns(tblLinkedBusinesses);
                    tablesRepository.CreateLinkedPersonsColumns(tblLinkedPersons);
                    tablesRepository.CreatePoliticalPositionsColumns(tblPoliticalPositions);

                    Dictionary<string, object> jsonResult = (Dictionary<string, object>)JSON.Deserializer<Dictionary<string, object>>(result.JsonResponse);

                    //Check that there were Matches located.
                    if (genericFunctions.CheckKeyExists(jsonResult, "matches") && Convert.ToInt32(genericFunctions.GetValueFromDictionary(jsonResult, "recordsFound")) > 0)
                    {
                        noOfResponses = Convert.ToInt32(genericFunctions.GetValueFromDictionary(jsonResult, "recordsFound"));

                        var matchesJson = (object)jsonResult.FirstOrDefault(pair => pair.Key == "matches");
                        var jsonItems = JSON.Serializer(matchesJson);
                        Dictionary<string, object> matchItems = (Dictionary<string, object>)JSON.Deserializer<Dictionary<string, object>>(jsonItems.ToString());

                        var matchesJson2 = (object)matchItems.FirstOrDefault(pair => pair.Key == "Value");
                        var jsonItems2 = JSON.Serializer(matchesJson2);
                        var jobj = JObject.Parse(jsonItems2);

                        //Fetch the JSON details for each record.
                        foreach (JProperty val in jobj.Children())
                        {
                            if (val.Name == "Value")
                            {
                                valArray = val.Children().Children().ToArray();
                                foreach (JToken item in valArray)
                                {
                                    PersonResponse response = new PersonResponse();
                                    var personJson = JSON.Serializer(item);

                                    //Assign the reponse details to the model Response.
                                    response = (PersonResponse)JSON.Deserializer<PersonResponse>(personJson);

                                    //Now we populate the Data Tables.
                                    tablesRepository.PopulatePerson(response, tblPerson);
                                    tablesRepository.PopulateAddresses(response, tblAddresses);
                                    tablesRepository.PopulateAlias(response, tblAlias);
                                    tablesRepository.PopulateArticles(response, tblArticles);
                                    tablesRepository.PopulateSanctions(response, tblSanctions);
                                    tablesRepository.PopulateNotes(response, tblNotes);
                                    tablesRepository.PopulateLinkedBusinesses(response, tblLinkedBusinesses);
                                    tablesRepository.PopulateLinkedPersons(response, tblLinkedPersons);
                                    tablesRepository.PopulatePoliticalPositions(response, tblPoliticalPositions);

                                    //Only construct html file if an email address is contained in the request or it is an Ftp request.
                                    if (emailAddress || ftp)
                                    {
                                        emailRepository.ConstructPersonEmail(response, newRecord);
                                        newRecord = false;
                                    }
                                }
                            }
                        }

                        emailRepository.FinishHtml();
                        htmlString = emailRepository.completeHtmlString;

                        updateTables = await sqlRepository.UpdateRiskAssessmentPersonTable(clientDetails.ClientId, tblPerson, tblAddresses, tblAlias, tblArticles, tblSanctions, tblNotes, tblLinkedBusinesses, tblLinkedPersons, tblPoliticalPositions);
                        if (updateTables)
                        {
                            if (tblPerson.Rows.Count > 0)
                            {
                                dictPerson.Add("Response", result.JsonResponse);
                            }
                            _logger.LogInformation("Person Search completed Successfully. ClientId={ClientId}", clientDetails.ClientId);
                            return dictPerson;
                        }
                        else
                        {
                            dictPerson.Add("Message", "Error in the method UpdateRiskAssessmentPersonTable, unable to add the search results to the system.");
                            return dictPerson;
                        }
                    }
                    else
                    {
                        dictPerson.Add("Message", "No records found for the selected values");
                        return dictPerson;
                    }
                }
                else
                {
                    _logger.LogError("Error retrieving the Person Search, Response {}. ClientId={ClientId}", result.Message, clientDetails.ClientId);
                    dictPerson.Add("Message", $"Error retrieving the Person Search, Response {result.Message}");
                    return dictPerson;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message, "Error performing the person search request. ClientId={ClientId}", clientDetails.ClientId);
                dictPerson.Add("Message", $"Error retrieving the Person Search, Response {ex.Message}");
                return dictPerson;
            }
        }

        /// <summary>
        /// Perform the Business/Bank Search.
        /// </summary>
        /// <param name="request"></param>
        /// <param name="clientDetails"></param>
        /// <param name="emailAddress"></param>
        /// <param name="searchType"></param>
        /// <returns></returns>
        public async Task<Dictionary<string, object>> BusinessSearch(BusinessSearch request, ClientDetails clientDetails, bool emailAddress, bool ftp, int searchType)
        {
            var dictBusiness = new Dictionary<string, object>();
            bool updateTables = false;
            bool newRecord = true;

            //Define the new Data Table instances.
            DataTable tblBusiness = new DataTable();
            DataTable tblAddresses = new DataTable();
            DataTable tblAlias = new DataTable();
            DataTable tblArticles = new DataTable();
            DataTable tblSanctions = new DataTable();
            DataTable tblNotes = new DataTable();
            DataTable tblLinkedBusinesses = new DataTable();
            DataTable tblLinkedPersons = new DataTable();

            //Instantiate a new instance of the class TablesRepository.
            TablesRepository tablesRepository = new TablesRepository(_creditSafeConfig, _logger);

            //Instantiate a new instance of the class SqlRepository.
            SqlRepository sqlRepository = new SqlRepository(_creditSafeConfig, _logger);

            //Instantiate a new instance of the class GenericFunctions.
            GenericFunctionsRepository genericFunctions = new GenericFunctionsRepository(_creditSafeConfig, _nrecoConfig, _logger);

            //Instantiate a new instance of the EmailRepository
            var emailRepository = new EmailRepository(_creditSafeConfig, _exchangeConfig, _nrecoConfig, _logger);

            try
            {
                var result = await RequestScorePayment(_creditSafeConfig.BusinessUrl, request, clientDetails, searchType==1 ? RequestType.Business : RequestType.Bank, _creditSafeConfig.BusinessApiKey);

                if (result.ValidResponse && result.JsonResponse != "")
                {
                    _logger.LogInformation("{} response received from Business Search. ClientId={ClientId}", result.ResponseCode, clientDetails.ClientId);

                    //Populate the Data Tables.
                    tablesRepository.CreateBusinessColumns(tblBusiness);
                    tablesRepository.CreateAddressColumns(tblAddresses);
                    tablesRepository.CreateAliasColumns(tblAlias);
                    tablesRepository.CreateArticlesColumns(tblArticles);
                    tablesRepository.CreateSanctionsColumns(tblSanctions);
                    tablesRepository.CreateNotesColumns(tblNotes);
                    tablesRepository.CreateLinkedBusinessesColumns(tblLinkedBusinesses);
                    tablesRepository.CreateLinkedPersonsColumns(tblLinkedPersons);

                    Dictionary<string, object> jsonResult = (Dictionary<string, object>)JSON.Deserializer<Dictionary<string, object>>(result.JsonResponse);
        
                    if (genericFunctions.CheckKeyExists(jsonResult, "matches") && Convert.ToInt32(genericFunctions.GetValueFromDictionary(jsonResult, "recordsFound")) > 0 )
                    {
                        noOfResponses = Convert.ToInt32(genericFunctions.GetValueFromDictionary(jsonResult, "recordsFound"));

                        var matchesJson = (object)jsonResult.FirstOrDefault(pair => pair.Key == "matches");
                        var jsonItems = JSON.Serializer(matchesJson);
                        Dictionary<string, object> matchItems = (Dictionary<string, object>)JSON.Deserializer<Dictionary<string, object>>(jsonItems.ToString());

                        var matchesJson2 = (object)matchItems.FirstOrDefault(pair => pair.Key == "Value");
                        var jsonItems2 = JSON.Serializer(matchesJson2);
                        var jobj = JObject.Parse(jsonItems2);

                        //Fetch the JSON details for each record.
                        foreach (JProperty val in jobj.Children())
                        {
                            if (val.Name == "Value")
                            {
                                valArray = val.Children().Children().ToArray();
                                foreach (JToken item in valArray)
                                {
                                    BusinessResponse response = new BusinessResponse();
                                    var businessJson = JSON.Serializer(item);

                                    //Assign the reponse details to the model Response.
                                    response = (BusinessResponse)JSON.Deserializer<BusinessResponse>(businessJson);

                                    //Now we populate the Data Tables.
                                    tablesRepository.PopulateBusiness(response, tblBusiness);
                                    tablesRepository.PopulateAddresses(response, tblAddresses);
                                    tablesRepository.PopulateAlias(response, tblAlias);
                                    tablesRepository.PopulateArticles(response, tblArticles);
                                    tablesRepository.PopulateSanctions(response, tblSanctions);
                                    tablesRepository.PopulateNotes(response, tblNotes);
                                    tablesRepository.PopulateLinkedBusinesses(response, tblLinkedBusinesses);
                                    tablesRepository.PopulateLinkedPersons(response, tblLinkedPersons);

                                    //Only construct html file if an email address is contained in the request or ftp is True.
                                    if (emailAddress || ftp)
                                    {
                                        emailRepository.ConstructBusinessEmail(response, newRecord);
                                        newRecord = false;
                                    }
                                }
                            }
                        }

                        emailRepository.FinishHtml();
                        htmlString = emailRepository.completeHtmlString;

                        updateTables = await sqlRepository.UpdateRiskAssessmentBusinessTable(clientDetails.ClientId, tblBusiness, tblAddresses, tblAlias, tblArticles, tblSanctions, tblNotes, tblLinkedBusinesses, tblLinkedPersons);
                        if (updateTables)
                        {
                            if (tblBusiness.Rows.Count > 0)
                            {
                                dictBusiness.Add("Response", result.JsonResponse);
                            }
                            _logger.LogInformation("Business Search completed Successfully. ClientId={ClientId}", clientDetails.ClientId);
                            return dictBusiness;
                        }
                        else
                        {
                            dictBusiness.Add("Message", "Error in the method UpdateRiskAssessmentBusinessTable, unable to add the search results to the system.");
                            return dictBusiness;
                        }
                    }
                    else
                    {
                        dictBusiness.Add("Message", "No records found for the selected values.");
                        return dictBusiness;
                    }
                }
                else
                {
                    _logger.LogError("Error retrieving the Business Search, Response {}. ClientId={ClientId}", result.Message, clientDetails.ClientId);
                    dictBusiness.Add("Message", $"Error retrieving the Person Search, Response { result.Message}");
                    return dictBusiness;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message, "Error performing the business search request. ClientId={ClientId}", clientDetails.ClientId);
                dictBusiness.Add("Message", $"Error performing the business search request {ex.Message}");
                return dictBusiness;
            }
        }

        private async Task<SearchResult> RequestScorePayment(string url, object request, ClientDetails clientDetails, string requestType, string apiKey)
        {
            //Instantiate a new instance of the class SqlRepository.
            SqlRepository sqlRepository = new SqlRepository(_creditSafeConfig, _logger);

            try
            {
                StringContent requestContent = null;
                
                using (var client = new HttpClient())
                {
                    //Serialise the object.
                    var payload = JSON.Serializer(request);

                    //Perform the asynchronous request.
                    client.DefaultRequestHeaders.Add("apiKey", apiKey);
                    requestContent = new StringContent(payload, Encoding.UTF8, "application/json");
                    responseMessage = await client.PostAsync(url, requestContent);
                }

                string responseBody = await responseMessage.Content.ReadAsStringAsync();
                var responseCode = Convert.ToInt32(responseMessage.StatusCode);

                if (responseMessage.IsSuccessStatusCode)
                {
                    _logger.LogInformation("Http Response {} | Response Code {}. ClientId={ClientId}", responseBody, responseCode, clientDetails.ClientId);

                    //Log the result into the table DolfinPaymentApiClientRequest to record the successful client request.
                    var result =  sqlRepository.InsertClientRequest(clientDetails.ClientId, clientDetails.ApiKey, requestType, RequestStatus.Completed);

                    SearchResult searchResult = new SearchResult
                    {
                        JsonResponse = responseBody,
                        ResponseCode = responseCode,
                        Message = $"Http Response {responseBody} | Response Code {responseCode}",
                        ValidResponse = true
                    };
                    return searchResult;
                }
                else
                {
                    ResponseBody response = (ResponseBody)JSON.Deserializer<ResponseBody>(responseBody);
                    _logger.LogInformation("Http Response {} | Response Code {}. ClientId={ClientId}", responseBody, responseCode, clientDetails.ClientId);

                    //Log the result into the table DolfinPaymentApiClientRequest to record the failed client request.
                    var result = sqlRepository.InsertClientRequest(clientDetails.ClientId, clientDetails.ApiKey, requestType, RequestStatus.Failed);

                    SearchResult searchResult = new SearchResult
                    {
                        JsonResponse = responseBody,
                        ResponseCode = responseCode,
                        Message = $"Http Response : {response.Message} | Response Code : {responseCode}",
                        ValidResponse = false
                    };
                    return searchResult;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message, "Error in RequestScorePayment. ClientId={ClientId}", clientDetails.ClientId);
                return null;
            }
        }

        public object FetchResponse()
        {
            return searchResponse;
        }
    }
}
