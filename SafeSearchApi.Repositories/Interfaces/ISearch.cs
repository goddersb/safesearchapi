﻿using SafeSearchApi.Models.Api;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SafeSearchApi.Repositories.Interfaces
{
    public interface ISearch
    {
        Task<Dictionary<string, object>> PersonSearch(PersonSearch personSearch, ClientDetails clientDetails, bool emailAddress, bool ftp);
        Task<Dictionary<string, object>> BusinessSearch(BusinessSearch businessSearch, ClientDetails clientDetails, bool emailAddress, bool ftp, int searchType);
        object FetchResponse();
    }
}
