﻿using SafeSearchApi.Models.Api;
using System.Threading.Tasks;

namespace SafeSearchApi.Repositories.Interfaces
{
    public interface IEmail
    {
        void BuildHtmlString(string title, string value, string htmlBody, bool titleHeader = false) { }
        public void ConstructPersonEmail(PersonResponse personResponse, bool newRecord) {}
        public void ConstructBusinessEmail(BusinessResponse businessResponse, bool newRecord) {}
        public void FinishHtml() { }
        public async Task<bool> SendEmail(ClientDetails clientDetails, string searchName) { return false; }
        public async Task<bool> SendAuthenticationEmail(ClientRequest clientRequest, ClientResponse clientResponse, string authKey) { return false; }
        public async Task<bool> SendAuthenticationEmail(ClientDetails clientDetails, string authKey) { return false; }
        public async Task<bool> SendConfirmationEmail(ClientDetails clientDetails) { return false; }
        public async Task<bool> SendNoResultsEmail(ClientDetails clientDetails, Request request, string searchName) { return false; }
    }
}