﻿using SafeSearchApi.Models.Api;
using SafeSearchApi.Models.Configuration;
using SafeSearchApi.Repositories.Interfaces;
using Microsoft.Extensions.Logging;
using System.Net;

namespace SafeSearchApi.Repositories
{
    public class FtpRepository : IFtp
    {
        private readonly FtpConfig _ftpConfig;
        private readonly ILogger _logger;

        public FtpRepository(FtpConfig ftpConfig, ILogger logger)
        {
            _ftpConfig = ftpConfig;
            _logger = logger;
        }

        public bool CreateClientFtpFolders(ClientDetails clientDetails, string folder) 
        {
            try
            {
                WebRequest ftpRequest = WebRequest.Create($"{folder}");
                ftpRequest.Method = WebRequestMethods.Ftp.MakeDirectory;
                ftpRequest.Credentials = new NetworkCredential(_ftpConfig.FtpUser, _ftpConfig.FtpPassword);

                try
                {
                    using (ftpRequest.GetResponse())
                    {
                        return true;
                    }
                }
                catch (WebException ex)
                {
                    _logger.LogError(ex.Message, "Error creating the Ftp folders. ClientId={ClientId}", clientDetails.ClientId);
                    return false;
                }
            }
            catch(WebException ex)
            {
                _logger.LogError(ex.Message, "Error creating the Ftp folders. ClientId={ClientId}", clientDetails.ClientId);
                return false;
            }
        }
    }
}