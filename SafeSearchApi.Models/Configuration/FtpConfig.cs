﻿namespace SafeSearchApi.Models.Configuration
{
    public class FtpConfig
    {
        public string FtpUser { get; set; }
        public string FtpPassword { get; set; }
        public string FtpUrl { get; set; }
        public string FtpServerFolder { get; set; }
        public string FtpClientFolder { get; set; }
        public string FtpProcessedFolder { get; set; }
        public string FtpFetchFileStoredProcedure { get; set; }
        public string FtpUpdateFileStoredProcedure { get; set; }
    }
}