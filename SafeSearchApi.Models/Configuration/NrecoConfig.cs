﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SafeSearchApi.Models.Configuration
{
    public class NrecoConfig
    {
        public string NrecoId { get; set; }
        public string NrecoKey { get; set; }
        public string NrecoToolPath { get; set; }
    }
}
