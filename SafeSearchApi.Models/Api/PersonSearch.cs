﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SafeSearchApi.Models.Api
{
    public class PersonSearch
    {
        public int threshold { get; set; }
        public bool? pep { get; set; }
        public bool? previousSanctions { get; set; }
        public bool? currentSanctions { get; set; }
        public bool? lawEnforcement { get; set; }
        public bool? financialRegulator { get; set; }
        public bool? insolvency { get; set; }
        public bool? disqualifiedDirector { get; set; }
        public bool? adverseMedia { get; set; }
        public string forename { get; set; }
        public string middlename { get; set; }
        public string surname { get; set; }
        public string dateOfBirth { get; set; }
        public string yearOfBirth { get; set; }
        public string address { get; set; }
        public string city { get; set; }
        public string county { get; set; }
        public string postCode { get; set; }
        public string country { get; set; }
    }
}
