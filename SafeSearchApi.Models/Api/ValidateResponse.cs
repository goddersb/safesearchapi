﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SafeSearchApi.Models.Api
{
    public class ValidateResponse
    {
        public string ResponseCode { get; set; }
        public string Message { get; set; }
    }
}
