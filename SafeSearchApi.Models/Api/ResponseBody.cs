﻿namespace SafeSearchApi.Models.Api
{
    public class ResponseBody
    {
        public string Message { get; set; }
    }
}
