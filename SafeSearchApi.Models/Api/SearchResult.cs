﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SafeSearchApi.Models.Api
{
    public class SearchResult
    {
        public string JsonResponse { get; set; }
        public int ResponseCode { get; set; }
        public string Message { get; set; }
        public bool ValidResponse { get; set; }
    }
}
