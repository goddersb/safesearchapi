﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SafeSearchApi.Models.Api
{
    public class FilePaths
    {
        public string PathName { get; set; }
        public string PathFilePath { get; set; }
    }
}
