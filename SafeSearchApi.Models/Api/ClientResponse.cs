﻿namespace SafeSearchApi.Models.Api
{
    public class ClientResponse
    {
        public string ClientId { get; set; }
        public string ApiKey { get; set; }
        public bool Validated { get; set; }
    }
}
